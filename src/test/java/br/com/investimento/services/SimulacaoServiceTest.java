package br.com.investimento.services;

import br.com.investimento.DTOs.CalculoSimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class SimulacaoServiceTest {

    @MockBean
    SimulacaoRepository simulacaoRepository;

    @Autowired
    SimulacaoService simulacaoService;

    Investimento investimento;
    Simulacao simulacao;
    CalculoSimulacaoDTO calculoSimulacaoDTO;
    List<Simulacao> listaSimulacoes;

    @BeforeEach
    private void setUp(){
        this.investimento = new Investimento();
        investimento.setNome("Poupança");
        investimento.setRendimentoAoMes(0.1);

        this.simulacao = new Simulacao();
        simulacao.setInvestimento(investimento);
        simulacao.setQuantidadeMeses(10);
        simulacao.setValorAplicado(new BigDecimal("50.00"));
        simulacao.setEmail("lucas@email.com");
        simulacao.setNomeInteressado("Lucas");
        simulacao.setId(1);

        this.calculoSimulacaoDTO = new CalculoSimulacaoDTO();
        calculoSimulacaoDTO.setRendimentoPorMes(new BigDecimal("5.00"));
        calculoSimulacaoDTO.setMontante(new BigDecimal("100.00"));

        this.listaSimulacoes = new ArrayList<>();
        listaSimulacoes.add(simulacao);
    }

    @Test
    public void testarSimularInvestimento(){
        Mockito.when(simulacaoService.calcularSimulacao(simulacao)).then(object -> object.getArgument(0));

        Assertions.assertEquals(calculoSimulacaoDTO, simulacaoService.calcularSimulacao(simulacao));
    }

    @Test
    public void testarConsultaTodasSimulacoes(){
        Mockito.when(simulacaoService.consultaTodasSimulacoes()).thenReturn(listaSimulacoes);

        Assertions.assertEquals(listaSimulacoes, simulacaoService.consultaTodasSimulacoes());
    }
}
