package br.com.investimento.services;

import br.com.investimento.DTOs.CadastroInvestimentoDTO;
import br.com.investimento.DTOs.CadastroSimulacaoDTO;
import br.com.investimento.DTOs.CalculoSimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTest {

    @MockBean
    private InvestimentoRepository investimentoRepository;

    @MockBean
    private SimulacaoService simulacaoService;

    @Autowired
    private InvestimentoService investimentoService;


    Investimento investimento;
    CadastroInvestimentoDTO investimentoDTO;
    List<Investimento> listaInvestimentos;
    CalculoSimulacaoDTO calculoSimulacaoDTO;
    CadastroSimulacaoDTO cadastroSimulacaoDTO;

    @BeforeEach
    private void setUp(){
        this.investimento = new Investimento();
        investimento.setNome("Poupança");
        investimento.setRendimentoAoMes(0.005);

        this.investimentoDTO = new CadastroInvestimentoDTO();
        investimentoDTO.setNome(investimento.getNome());
        investimentoDTO.setRendimentoAoMes(new BigDecimal("0.005"));

        listaInvestimentos = new ArrayList<>();
        listaInvestimentos.add(investimento);

        this.calculoSimulacaoDTO = new CalculoSimulacaoDTO();
        calculoSimulacaoDTO.setMontante(new BigDecimal("1000.02"));
        calculoSimulacaoDTO.setRendimentoPorMes(new BigDecimal("2.60"));

        this.cadastroSimulacaoDTO = new CadastroSimulacaoDTO();
        cadastroSimulacaoDTO.setEmail("lucas@email.com");
        cadastroSimulacaoDTO.setNomeInteressado("Lucas");
        cadastroSimulacaoDTO.setQuantidadeMeses(5);
        cadastroSimulacaoDTO.setValorAplicado(new BigDecimal("500.00"));
    }

    @Test
    public void testarCriarNovoInvestimento(){
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).then(object -> object.getArgument(0));

        Assertions.assertEquals(investimento, investimentoService.cadastrarNovoInvestimento(investimentoDTO));
    }

    @Test
    public void testarCriarNovoInvestimentoException(){
        Mockito.when(investimentoRepository.findByNome(Mockito.anyString())).thenReturn(Optional.of(investimento));

        Assertions.assertThrows(RuntimeException.class, () -> {investimentoService.cadastrarNovoInvestimento(investimentoDTO);});
    }

    @Test
    public void testarConsultaInvestimentosPossiveis(){
        Mockito.when(investimentoRepository.findAll()).thenReturn(listaInvestimentos);

        Assertions.assertEquals(listaInvestimentos, investimentoService.listarInvestimentosPossiveis());
    }

    @Test
    public void testarConsultaInvestimentosPossiveisSemInvestimentos(){
        List<Investimento> listaVazia = new ArrayList<>();
        Mockito.when(investimentoRepository.findAll()).thenReturn(listaVazia);

        Assertions.assertEquals(listaVazia, investimentoService.listarInvestimentosPossiveis());
    }

    @Test
    public void testarSimularInvestimento(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(investimento));
        Mockito.when(simulacaoService.calcularSimulacao(Mockito.any(Simulacao.class))).thenReturn(calculoSimulacaoDTO);

        Assertions.assertEquals(calculoSimulacaoDTO, investimentoService.simularInvestimento(cadastroSimulacaoDTO, 0));
    }

    @Test
    public void testarSimularInvestimentoComInvestimentoInexistente(){
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class, () -> {investimentoService.simularInvestimento(cadastroSimulacaoDTO,0);});
    }

}
