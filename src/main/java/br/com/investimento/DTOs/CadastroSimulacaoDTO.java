package br.com.investimento.DTOs;

import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public class CadastroSimulacaoDTO {

    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser em branco")
    @Size(min = 3, message = "Nome do interessado deve possuir no mínimo 3 caractéres.")
    private String nomeInteressado;

    @Email(message = "E-mail inválido")
    private String email;

    @NotNull(message = "Valor aplicado não pode ser nulo")
    @Digits(integer = 1000000000, fraction = 2, message = "Valor aplicado deve ser digitado com no máximo 2 casas decimais")
    @NumberFormat(pattern = "#0.00")
    private BigDecimal valorAplicado;

    @NotNull(message = "Quantidade de meses não pode ser nulo")
    @Min(value = 1, message = "Quantidade de meses deve ser maior que 0 (zero)")
    private int quantidadeMeses;

    public CadastroSimulacaoDTO() {
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(BigDecimal valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Simulacao converterParaSimulacao(Investimento investimento){
        Simulacao simulacao = new Simulacao();
        simulacao.setInvestimento(investimento);
        simulacao.setNomeInteressado(this.nomeInteressado);
        simulacao.setEmail(this.email);
        simulacao.setValorAplicado(this.valorAplicado);
        simulacao.setQuantidadeMeses(this.quantidadeMeses);

        return simulacao;
    }
}
