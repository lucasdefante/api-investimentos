package br.com.investimento.DTOs;

import br.com.investimento.models.Investimento;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CadastroInvestimentoDTO {

    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome não pode estar em branco")
    @Size(min = 3, message = "Nome de investimento deve possuir no mínimo 3 caractéres.")
    private String nome;

    @NotNull(message = "Rendimento ao mês não pode ser nulo")
    @Digits(integer = 100, fraction = 4, message = "Rendimento deve ser digitado com no máximo 4 casas decimais")
    @NumberFormat(pattern = "#0.0000")
    private BigDecimal rendimentoAoMes;

    public CadastroInvestimentoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(BigDecimal rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public Investimento converterParaInvestimento(){
        Investimento investimento = new Investimento();
        investimento.setNome(this.getNome());
        investimento.setRendimentoAoMes(this.getRendimentoAoMes().doubleValue());
        return investimento;
    }
}
