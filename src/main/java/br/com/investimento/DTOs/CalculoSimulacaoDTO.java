package br.com.investimento.DTOs;

import org.springframework.format.annotation.NumberFormat;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class CalculoSimulacaoDTO {

    private BigDecimal rendimentoPorMes;
    private BigDecimal montante;

    public CalculoSimulacaoDTO() {
    }

    public BigDecimal getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(BigDecimal rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes.setScale(2, RoundingMode.FLOOR);
    }

    public BigDecimal getMontante() {
        return montante;
    }

    public void setMontante(BigDecimal montante) {
        this.montante = montante.setScale(2, RoundingMode.FLOOR);

    }

    @Override
    public String toString() {
        return "{" +
                "rendimentoPorMes=" + rendimentoPorMes +
                ", montante=" + montante +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalculoSimulacaoDTO that = (CalculoSimulacaoDTO) o;
        return Objects.equals(rendimentoPorMes, that.rendimentoPorMes) &&
                Objects.equals(montante, that.montante);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rendimentoPorMes, montante);
    }
}
