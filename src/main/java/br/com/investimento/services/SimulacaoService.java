package br.com.investimento.services;

import br.com.investimento.DTOs.CalculoSimulacaoDTO;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;


    public CalculoSimulacaoDTO calcularSimulacao(Simulacao simulacao) {
        CalculoSimulacaoDTO calculoSimulacaoDTO = new CalculoSimulacaoDTO();
        Double rendimentoMensal = 0.0;
        Double montante = 0.0;

        // Juros Simples
        rendimentoMensal = simulacao.getInvestimento().getRendimentoAoMes() * simulacao.getValorAplicado().doubleValue();
        montante = simulacao.getValorAplicado().doubleValue() + (rendimentoMensal * simulacao.getQuantidadeMeses());

        calculoSimulacaoDTO.setRendimentoPorMes(new BigDecimal(rendimentoMensal));
        calculoSimulacaoDTO.setMontante(new BigDecimal(montante));

        simulacaoRepository.save(simulacao);

        return calculoSimulacaoDTO;
    }

    public Iterable<Simulacao> consultaTodasSimulacoes() {
        return simulacaoRepository.findAll();
    }
}
