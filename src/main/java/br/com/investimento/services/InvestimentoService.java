package br.com.investimento.services;

import br.com.investimento.DTOs.CadastroInvestimentoDTO;
import br.com.investimento.DTOs.CadastroSimulacaoDTO;
import br.com.investimento.DTOs.CalculoSimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    public Investimento cadastrarNovoInvestimento(CadastroInvestimentoDTO investimentoDTO) {
        if(investimentoRepository.findByNome(investimentoDTO.getNome()).isPresent()){
            throw new RuntimeException("Já existe investimento com o mesmo nome");
        }

        Investimento investimento = investimentoDTO.converterParaInvestimento();

        return investimentoRepository.save(investimento);
    }

    public Iterable<Investimento> listarInvestimentosPossiveis() {
        return investimentoRepository.findAll();
    }

    public CalculoSimulacaoDTO simularInvestimento(CadastroSimulacaoDTO cadastroSimulacaoDTO, int idInvestimento){
        Optional<Investimento> investimento = investimentoRepository.findById(idInvestimento);
        if(!investimento.isPresent()){
            throw new RuntimeException("ID de investimento inválido");
        }

        Simulacao simulacao = cadastroSimulacaoDTO.converterParaSimulacao(investimento.get());
        CalculoSimulacaoDTO calculoSimulacaoDTO = simulacaoService.calcularSimulacao(simulacao);

        return calculoSimulacaoDTO;
    }


}
