package br.com.investimento.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "investimento")
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    private String nome;
    private Double rendimentoAoMes;

    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(Double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Investimento that = (Investimento) o;
        return id == that.id &&
                Objects.equals(nome, that.nome) &&
                Objects.equals(rendimentoAoMes, that.rendimentoAoMes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, rendimentoAoMes);
    }

    @Override
    public String toString() {
        return "Investimento{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", rendimentoAoMes=" + rendimentoAoMes +
                '}';
    }
}
