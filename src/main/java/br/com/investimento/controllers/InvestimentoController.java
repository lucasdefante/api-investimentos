package br.com.investimento.controllers;

import br.com.investimento.DTOs.CadastroInvestimentoDTO;
import br.com.investimento.DTOs.CadastroSimulacaoDTO;
import br.com.investimento.DTOs.CalculoSimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento cadastrarInvestimento(@RequestBody @Valid CadastroInvestimentoDTO cadastroInvestimentoDTO){
        try {
            return investimentoService.cadastrarNovoInvestimento(cadastroInvestimentoDTO);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{id}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public CalculoSimulacaoDTO simularInvestimento(@RequestBody @Valid CadastroSimulacaoDTO cadastroSimulacaoDTO,
                                                   @PathVariable(name = "id") int idInvestimento){
        try {
            return investimentoService.simularInvestimento(cadastroSimulacaoDTO, idInvestimento);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public Iterable<Investimento> listarInvestimentosPossiveis(){
        return investimentoService.listarInvestimentosPossiveis();
    }
}
